
subroutine write_temporary(version, nx, ny, ounit, iproj, nlats, xfcst, xlvl, &
                           startlat, startlon, deltalat, deltalon, dx, dy, xlonc, &
                           truelat1, truelat2, earth_radius, slab, is_wind_grid_rel, &
                           startloc, field, hdate, units, map_source, desc)
integer :: version      ! Format version (must =5 for WPS format)
integer :: nx, ny       ! x-and y-dimensions of 2-d array
integer :: ounit        ! output unit
integer :: iproj        ! Code for projection of data in array:
                        ! 0 = cylindrical equidistant
                        ! 1 = Mercator
                        ! 3 = Lambert conformal conic
                        ! 4 = Gaussian (global only!) 
                        ! 5 = Polar stereographic
real :: nlats           ! Number of latitudes north of equator 
                        ! (for Gaussian grids)
real :: xfcst           ! Forecast hour of data
real :: xlvl            ! Vertical level of data in 2-d array
real :: startlat, startlon
                        ! Lat/lon of point in array indicated by 
                        ! startloc string
real :: deltalat, deltalon ! Grid spacing, degrees
real :: dx, dy          ! Grid spacing, km
real :: xlonc           ! Standard longitude of projection
real :: truelat1, truelat2 ! True latitudes of projection
real :: earth_radius    ! Earth radius, km
real, dimension(nx,ny) :: slab! The 2-d array holding the data
logical :: is_wind_grid_rel ! Flag indicating whether winds are 
                        ! relative to source grid (TRUE) or 
                        ! relative to earth (FALSE)
character (len=8)  :: startloc ! Which point in array is given by startlat/startlon; set either 
                        ! to 'SWCORNER' or 'CENTER  '
character (len=9)  :: field ! Name of the field
character (len=24) :: hdate ! Valid date for data YYYY:MM:DD_HH:00:00
character (len=24) :: dateout ! Valid date for WPS expectations YYYY-MM-DD_HH
character (len=25) :: units! Units of data
character (len=32) :: map_source  !  Source model / originating center
character (len=46) :: desc ! Short description of data

!f2py intent(in) :: version, nx, ny, ounit, iproj, nlats, xfcst, xlvl, stratlat, stratlon, deltalat, deltalon, dx, dy, xlonc, truelat1, truelat2, earth_radius, slab, is_wind_grid_rel, startloc, field, hdate, units, map_source, desc

dateout = hdate(1:4)//'-'//hdate(6:7)//'-'//hdate(9:13)
open(unit=ounit,form='unformatted',access='sequential', &
     convert='big_endian',file=field(1:len( trim(field) ))//':'//dateout)

!  1) WRITE FORMAT VERSION
write(unit=ounit) version

!  2) WRITE METADATA
        ! Cylindrical equidistant
if (iproj == 0) then
write(unit=ounit) hdate, xfcst, map_source, field, &
                  units, desc, xlvl, nx, ny, iproj
write(unit=ounit) startloc, startlat, startlon, &
                  deltalat, deltalon, earth_radius
        ! Mercator
else if (iproj == 1) then
write(unit=ounit) hdate, xfcst, map_source, field, &
                  units, desc, xlvl, nx, ny, iproj
write(unit=ounit) startloc, startlat, startlon, dx, dy, &
                  truelat1, earth_radius
        ! Lambert conformal
else if (iproj == 3) then
write(unit=ounit) hdate, xfcst, map_source, field, &
                  units, desc, xlvl, nx, ny, iproj
write(unit=ounit) startloc, startlat, startlon, dx, dy, &
                  xlonc, truelat1, truelat2, earth_radius
        ! Gaussian
else if (iproj == 4) then
write(unit=ounit) hdate, xfcst, map_source, field, &
                  units, desc, xlvl, nx, ny, iproj
write(unit=ounit) startloc, startlat, startlon, &
                  nlats, deltalon, earth_radius
        ! Polar stereographic
else if (iproj == 5) then
write(unit=ounit) hdate, xfcst, map_source, field, &
                  units, desc, xlvl, nx, ny, iproj
write(unit=ounit) startloc, startlat, startlon, dx, dy, &
                  xlonc, truelat1, earth_radius
end if

!  3) WRITE WIND ROTATION FLAG 
write(unit=ounit) is_wind_grid_rel


!  4) WRITE 2-D ARRAY OF DATA
write(unit=ounit) slab
END SUBROUTINE
END

