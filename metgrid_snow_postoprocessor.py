import netCDF4
import numpy as np
import argparse
import glob
import xarray as xr
import subprocess


def add_snow_mask(met_em, snoalb_modis="ALB_MODIS"):
    ds = xr.open_dataset(met_em)
    if snoalb_modis in ds.data_vars.keys():
        snowc = ds["LANDMASK"].copy()
        snowc.values = (ds[snoalb_modis] > 0).astype(int)
        snowc.attrs["description"] = "Snow cover from albedo modis"
        ds["SNOWC"] = snowc
        ds.to_netcdf(met_em + "tmp", mode="w")
        subprocess.call(["mv", met_em + "tmp", met_em])
        ds.close()
    else:
        raise ValueError(f"field {snoalb_modis} not found in {met_em}")
    return None


def add_snowtime(met_em, snoalb_modis="ALB_MODIS"):
    # Define constants
    LVCOEF = 0.5
    SNACCA = 0.82  # 0.94
    SNACCB = 0.46  # 0.58
    # Open dataset
    ds = xr.open_dataset(met_em)
    # Copy an existing similar field to get some attributes for free
    snotime = ds["SNOWC"].copy()
    snotime.values = np.zeros(snotime.values.shape)
    # get snow cover
    snow_mask = ds["SNOWC"].astype(int).squeeze()
    # get snoalb modis where it is defined only
    alb_modis = ds[snoalb_modis].squeeze().where(snow_mask) / 100
    snoalb1 = (ds["SNOALB"] / 100 + LVCOEF * (0.85 - ds["SNOALB"] / 100)).squeeze()
    ta = np.log(alb_modis / snoalb1)
    #    ta = np.log(alb_modis / (ds["SNOALB"]/100).squeeze())
    tb = ta / np.log(SNACCA)
    # Change tb to float64 otherwise it raises errors
    # tb needs to > 0 to get real values
    tmp = 86400 * np.power(tb.astype("float64").where(tb > 0), (1 / SNACCB))
    tmp.values[np.isnan(tmp)] = 0
    # Store it in the ncdf
    snotime.values[0, :] = tmp
    snotime.attrs["description"] = "Number of Seconds Since the Last Snowfall"
    ds["SNOTIME"] = snotime
    ds.to_netcdf(met_em + "tmp", mode="w")
    subprocess.call(["mv", met_em + "tmp", met_em])
    ds.close()
    return None


def add_snowh(met_em):
    # set constants
    SNOWH_0000 = 0.05
    SNOWH_0500 = 0.15
    SNOWH_5000 = 1.50
    DSNOWH_DZ_0500_0000 = (SNOWH_0500 - SNOWH_0000) / 500
    DSNOWH_DZ_5000_0500 = (SNOWH_5000 - SNOWH_0500) / 4500
    # Open dataset
    ds = xr.open_dataset(met_em)
    # Copy an existing similar field to get some attributes for free
    snowh = ds["SNOWC"].copy()
    hgt = ds["HGT_M"].values.squeeze()
    tmp = np.zeros(hgt.shape)
    snowc = ds["SNOWC"].values.squeeze()
    # Make the computation for under than 500m
    tmp[hgt >= 500] = snowc[hgt >= 500] * (
        DSNOWH_DZ_5000_0500 * hgt[hgt >= 500] + SNOWH_0500
    )
    tmp[hgt < 500] = snowc[hgt < 500] * (
        DSNOWH_DZ_0500_0000 * hgt[hgt < 500] + SNOWH_0000
    )
    # Write informations into the netcdf
    snowh.values[0, :] = tmp
    snowh.attrs["description"] = "Snow height in meters"
    ds["SNOWH"] = snowh
    ds.to_netcdf(met_em + "tmp", mode="w")
    subprocess.call(["mv", met_em + "tmp", met_em])
    ds.close()
    return None


def update_snow(met_em):
    # set constants
    RHOSN_MAX = 100  # [kg/m3]
    A = 0.873512
    B = 0.0812197
    # Open dataset
    ds = xr.open_dataset(met_em)
    snotime = ds["SNOTIME"].values.squeeze()
    snowh = ds["SNOWH"].values.squeeze()
    # Copy an existing similar field to get some attributes for free
    snow = ds["SNOW"].copy()
    rhosn = RHOSN_MAX * np.exp(A * np.power(snotime / 86400, B))
    ds["SNOW"].values[0, :] = rhosn * snowh
    ds.to_netcdf(met_em + "tmp", mode="w")
    subprocess.call(["mv", met_em + "tmp", met_em])
    ds.close()
    return None


def postprocess_met_em_snow(met_em, snoalb_modis="ALB_MODIS"):
    print(f"Processing {met_em}")
    print(f"  -> Adding SNOWC (snow mask)")
    add_snow_mask(met_em, snoalb_modis="ALB_MODIS")
    print(f"  -> Adding SNOTIME (time since last snowfall)")
    add_snowtime(met_em, snoalb_modis="ALB_MODIS")
    print(f"  -> Adding SNOWH (snow height)")
    add_snowh(met_em)
    print(f"  -> Updating SNOW (snow water equivalent)")
    update_snow(met_em)
    return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "met_ems", help="Wildcard exprression of the met_em files to be postprocessed"
    )
    args = parser.parse_args()
    print(f"Script to postprocess metgrid files to add snow related variables")
    # List all files to be postprocessed
    met_ems = glob.glob(args.met_ems)
    # Processing them one by one
    for met_em in met_ems:
        postprocess_met_em_snow(met_em)
