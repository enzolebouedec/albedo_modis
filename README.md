# ALBEDO_MODIS
Repository which intend to describe a full procedure to use albedo information
from the modis dataset with WPS. To be able to follow this procedure one need
a fortran compiler and a python installation.

_Note to Charles: if you have a conda environment with only the packages needed
to follow the procedure, the easiest might me to integrate the .yml file
describing the environment in repository : conda env export > environment.yml_

## Full precedure

### 1st step (to do only once)
Note: most of the WRF compilations expect binaries to be in the big endian
convention. However we have encountered situations where it is not the
case. To check what is expected by your exectuables, run the `check_endian.py` 
program on one temporary file written by ungrib.

```
python check_endian.py BINARY_FILE
```

The endianness of the file will correspond to the one that outputs a 3,4 or 5
for the first integer value.

Use f2py to convert the fortran function "make_intermediate.f90"
```
bash make_f2py.sh big
```
```
bash make_f2py.sh little
```
### 2nd step
Download data from [Modis website][2].

Be careful to download enough data to cover all the domains !

### 3rd step
Run the bash script to make VRTs (virtual format used by GDAL) combining all
tiles corresponding to a single day. These VRTs will then be transformed into
TIFs files and reprojected on a regular LatLon grid.
```
bash convert_hdf_to_tifs.sh Start_dayofyear Start_year End_dayofyear End_year
```
```
bash convert_hdf_to_tifs.sh 355 2016 366 2016
```

### 4th step
Run the python script to make temporary files for WPS.
Dates should be provided on the mm/dd/yyyy format to avoid confusion.

A valid example is:
```
python make_temporary_files.py  12/25/2016 12/28/2016 -a 5
```
The idea of this script is to reconstruct modis albedo files using the current 
date and a certain number of previous dates (given by the argument -a). At
every time step, if a value of higher priority is found, it will replace the
previous value. Orders of priority are defined as:
```
Missing values < No snow < Valid albedos
```
Where 
- Missing values
    - 101 : no decision
    - 111 : night
    - 150 : cloud
    - 151 : cloud detected as snow
    - 250 : missing
    - 251 : self-shadowing
- No snow
    - 125 : land
    - 137 : inland water
    - 139 : ocean
    - 252 : landmask mismatch
    - 253 : BRDF failure
    - 254 : non production mask
- Valid albedos
    - [1-100]

If informations of the same priority level are compared, the most recent one
will be kept.

In the output field, 255 corresponds to missing values, 0 to no snow and all 
values between 1 and 100 to valid albedos. 

Metadata for the binary will be read by the python program using gdalinfo.

### 5th step
in namelist.wps add ALB_MODIS and/or ALB_AGE to the line fg_name before
executing metgrid.exe.
```
sed -i "/.*fg_name*/c\ fg_name  ='FILE_SFC','FILE_PL','ALB_MODIS','ALB_AGE'" namelist.wps
```

### 6th step
In METGRID.TBL add the following lines:
```text
========================================
name=ALB_MODIS
        interp_option=four_pt+wt_average_four_pt
        fill_missing=0
        missing_value=255
        flag_in_output=FLAG_MODIS
========================================
name=ALB_AGE
        interp_option=four_pt+wt_average_four_pt
        fill_missing=-1
        missing_value=-1
        flag_in_output=FLAG_MODIS_AGE
========================================
```
The best interpolation option is still to be determined. All options available
are described in the [WRF userguide][1]. So far I would favor the "four_pt"
option because values are kept on the same bound whereas "sixteen_pt+four_pt"
can lead to albedo greater than 100 or smaller smaller than 0.


### 7th step
Execute metgrid

### 8th step
```
python metgrid_snow_postprocessor.py "met_em.d04.2016-12-2*_00:00:00.nc"
```

Post process the met_em files to create or update some snow related variables:
* SNOWC : mask of snow cover (ADDED)
 Boolean flag whether there is a valid snow albedo or not
* SNOW : snow water equivalent (MODIFIED)
 ```math
 SNOW = \rho_s * SNOWH
 ```
 where $`\rho_s=f \left( SNOTIME \right)`$ is an empirical function determined
 through numerical fitting of experimental values from [_Meloysund et al._, 2007][4]:
 ```math
 \rho_s = \rho_{sm} e^{A \times ( SNOTIME / 86400 )^B )}
 ```
 with $`\rho_{sm}=100`$ kg/m3, $`A=0.873512`$ et $`B = 0.0812197.`$.
* SNOWH : snow height (ADDED)
 This variable is also being determined an empirical function:

```math
\text{SNOWH} = \text{SNOWC} \left( \frac{d \left(\text{SNOWH}\right) }{dZ}_{500-0} \times \text{HGT\_M} + \text{SNOWH}_{0} \right) \text{    if HGT\_M} \le 500 \text{ m.a.s.l}
```
```math
\text{SNOWH} = \text{SNOWC} \left( \frac{d\left(\text{SNOWH}\right)}{dZ}_{5000-500} \times \text{HGT\_M} + \text{SNOWH}_{500} \right) \text{    if HGT\_M} \geq 500 \text{ m.a.s.l}
```

where
```math
\text{SNOWH}_0 = 0.05\text{ m, SNOWH}_{500} = 0.15\text{ m, SNOWH}_{5000} = 1.50\text{ m.}
```

and 
```math
\frac{d\left(\text{SNOWH}\right)}{dZ}_{500-0} = (\text{SNOWH}_{500} - \text{SNOWH}_{0000}) / 500
```
```math
\frac{d\left(\text{SNOWH}\right)}{dZ}_{5000-500} = ( \text{SNOWH}_{5000} - \text{SNOWH}_{0500}) / 4500
```

* SNOTIME : Time since last snowfall in seconds (ADDED)
This variable is computed by inverting equation (1a) given in [_Livneh et al_, 2010][3]:

```math
\text{SNOTIME} = 86400 \times \left( \frac{ln\left(\frac{\text{ALB\_MODIS}}{ \text{SNOALB1}} \right)}{ln \left( \text{A} \right)} \right) ^{ \frac{1}{\text{B}} } \text{    if SNOTIME is real}
```
```math
\text{SNOTIME} = 0 \text{    otherwise}
```
With :
```math
SNOALB1 = SNOALB + C * ( 0.85 - SNOALB )
```

where ALB\_MODIS is the albedo from the MODIS dataset and SNOALB the albedo 
derived from the climatological values and already present in WPS terrestrial
data. Regarding the coefficients we have A and B being equal to 0.94 
and 0.58 (0.82 and 0.46), respectively, during accumulation (ablation) phase.
Finally we also have C=0.5.



[1]: http://www2.mmm.ucar.edu/wrf/users/docs/user_guide_V3.9/ARWUsersGuideV3.9.pdf
[2]: https://nsidc.org/data/MOD10A1/versions/6
[3]: https://journals-ametsoc-org.gaelnomade-2.grenet.fr/doi/full/10.1175/2009JHM1174.1
[4]: https://rmets-onlinelibrary-wiley-com.gaelnomade-2.grenet.fr/doi/abs/10.1002/met.40
